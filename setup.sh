#!/bin/bash

# Colours
NOTIFY='\033[0;33m'
SUCCESS='\033[0;32m'
ALERT='\033[0;31m'
RESET='\033[0m'

# Read in Environment Variables
source ./.env

# Build and start the developement container.
echo -e "${NOTIFY}Build and start the development environment.${RESET}"
docker-compose up -d --build 

# Run the initial Composer install to set up Symfony etc.
echo -e "${NOTIFY}Set up Symfony.${RESET}"
docker exec -i -u 1000 $CONTAINER_NAME composer install

# Set up the phpcs configuration
echo -e "${NOTIFY}Set phpcs defaults.${RESET}"
cp ./CodeSniffer.conf ./development/vendor/squizlabs/php_codesniffer/CodeSniffer.conf

# Done
echo -e "${SUCCESS}Setup Complete. Ready for development.${RESET}"